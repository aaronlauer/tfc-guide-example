provider "aws" {
  region = var.region
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

module "vpc" {
  version = "3.11.3"

  source                            = "terraform-aws-modules/vpc/aws"
  name                              = "tribal-data-platform"
  cidr                              = "10.0.0.0/16"
  azs                               = ["${var.region}a", "${var.region}b", "${var.region}c"]
  enable_dns_hostnames              = true
  enable_dns_support                = true
  enable_vpn_gateway                = false
  propagate_public_route_tables_vgw = false
  create_database_subnet_group      = false
  enable_nat_gateway                = false

  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  tags = {
    Terraform   = "true"
    Environment = "tfc-guide-example"
    Owner       = "aaron.lauer"
  }
}

resource "aws_security_group" "demo_servers" {
  provider = aws
  name = "demo-servers"
  description = "For demo servers"
  vpc_id = "${module.vpc.vpc_id}"
}

resource "aws_security_group_rule" "allow_access_aws_ssh" {
  provider = aws
  type = "ingress"
  to_port = 22
  from_port = 22
  protocol = "tcp"
  cidr_blocks = ["223.25.73.197/32","34.98.250.119/32"] #home and office vpn
  security_group_id = "${aws_security_group.demo_servers.id}"
}

resource "aws_key_pair" "deployer" {
  key_name   = "aaron-dev"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDEz06WhEkoDVDRt3bvkPxhFRTdtnp7fzbjDwDte3T2081/AKjRvztRqzokJKE8aPGG3A5P9WoZuNXyo5SZ/AreJbzDvl0CtHzpRX/DznEkJ2J74e76FDr8Ob/xHs25y9TOSxnZvgufSs/Zf7x/AaIdEbnZk/j6LFsoZXdvrtdsLOlCl8Art4wK4zHxY//B3O9h4Pe/zBZqkGWH7l9PegdaC2SCf1RTlJTPMkPK+1t1jxoEvK3xIHMuD7HhkREuuJYnOvJBGuwDqEFjTmhTepQqRd9gJFqPWZcerWfW8og4uoJPgU0xl7tqRDkYsGpIdljIIvuzPnz9ImR18kAzFhYj aaron@LAPTOP-I7MRQ3VN"
}

module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = var.instance_name

  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_type
  key_name               = resource.aws_key_pair.deployer.key_name
  monitoring             = true
  vpc_security_group_ids = [resource.aws_security_group.demo_servers.id]
  subnet_id              = module.vpc.public_subnets[0]

  tags = {
    Terraform   = "true"
    Environment = "tfc-guide-example"
    Owner       = "aaron.lauer"
  }
}
