terraform {
  required_version = ">= 1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
    }

    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
  }
  cloud {
    organization = "home-projects"
    workspaces {
      name = "tfc-guide-example"
    }
  }
}
